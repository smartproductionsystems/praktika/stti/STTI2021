package org.htwdd.opcua.server;

import com.prosysopc.ua.server.UaServer;

public class DefaultServer implements ServerConfiguration {
	
	
	
	private int port;
	private String serverName;

	/**
	 * 
	 * @param port
	 * @param serverName
	 */
	public DefaultServer(
			final int port,
			final String serverName
			) {
		this.port = port;
		this.serverName = serverName;		
	}
	

	@Override
	public UaServer configure() {
		UaServer server = new UaServer();
	
        return server;	
	}

}
