package org.htwdd.opcua.server;

import com.prosysopc.ua.server.UaServer;

public class UserPasswordServerWrapper implements ServerConfiguration {

	private final ServerConfiguration defaultServer;
	
	/**
	 * 
	 * @param defaultServer
	 * DefaultServer configuration
	 */
	public UserPasswordServerWrapper(final ServerConfiguration defaultServer){
		this.defaultServer = defaultServer;
	}
	
	@Override
	public UaServer configure() {
		UaServer server = defaultServer.configure();
		/*
		 * steps:
		 * rmeove the anonymous usertokenpolicy
		 * add usertokenpolicy for user/password auth
		 * add an uservalidator
		 */
		
		return server;	
		
	}	
	

	
}
