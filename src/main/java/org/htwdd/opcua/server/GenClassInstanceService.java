package org.htwdd.opcua.server;

import com.prosysopc.ua.server.UaServer;

public class GenClassInstanceService implements ServerConfiguration {

	
	private BasicServer basicServer;
	private String instanceName;

	/** 
	 * 
	 * @param basicServer
	 * @param instanceName
	 */
	public GenClassInstanceService(BasicServer basicServer, String instanceName) {
		this.basicServer = basicServer;
		this.instanceName = instanceName;
	}
	
	
	@Override
	public UaServer configure() {
		/*
		 * steps:
		 * import the xml model
		 * get objectsfolder from NodeManager
		 * create instance of your designed class
		 * add the instance as a component to the Objectsfolder
		 * 
		 */
	
		return null;
	}

	
}
