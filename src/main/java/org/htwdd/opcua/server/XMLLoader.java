package org.htwdd.opcua.server;

import com.prosysopc.ua.server.UaServer;

public class XMLLoader {

	private ServerConfiguration basicServer;


	public XMLLoader(final ServerConfiguration basicServer) {
		this.basicServer = basicServer;
	}
	
	
	public UaServer ImportXMLModel(String xmlFileName) {
		
		
		UaServer server = basicServer.configure();
		
		
		return server;

		
	}
}
