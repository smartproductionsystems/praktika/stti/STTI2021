package org.htwdd.opcua.server;

import com.prosysopc.ua.server.UaServer;

public interface ServerConfiguration {
	public UaServer configure();
}
