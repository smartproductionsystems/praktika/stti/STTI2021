package org.htwdd.opcua.client;

import java.net.URISyntaxException;

import com.prosysopc.ua.ServiceException;
import com.prosysopc.ua.client.UaClient;
import com.prosysopc.ua.client.nodes.UaClientNodeFactory;
import com.prosysopc.ua.stack.transport.security.SecurityMode;

public class BasicClientConfigurer implements ClientConfigurator{

	private final String serverUri;
	UaClient client;
	public BasicClientConfigurer(final String serverUri) {
		
		this.serverUri = serverUri;
	}
	
	public UaClient configure(UaClient client) {
		//TODO: return a client which is configured with serverUri and lowest security mode	
		return client;
	}

	@Override
	public UaClient getConfiguredClient() {
		// TODO Auto-generated method stub
		return client;
	}	
	
	
}
