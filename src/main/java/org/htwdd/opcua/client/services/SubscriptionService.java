package org.htwdd.opcua.client.services;

import org.htwdd.opcua.client.ClientConfigurator;

import com.prosysopc.ua.ServiceException;
import com.prosysopc.ua.StatusException;
import com.prosysopc.ua.client.AddressSpaceException;
import com.prosysopc.ua.client.MonitoredDataItem;
import com.prosysopc.ua.client.MonitoredDataItemListener;
import com.prosysopc.ua.client.Subscription;
import com.prosysopc.ua.client.UaClient;
import com.prosysopc.ua.nodes.UaNode;
import com.prosysopc.ua.stack.builtintypes.DataValue;
import com.prosysopc.ua.stack.builtintypes.NodeId;

public class SubscriptionService extends BaseUaService  {

	
	

	public SubscriptionService(ClientConfigurator config) {
		super(config);
		// TODO Auto-generated constructor stub
	}

	public UaClient subscribe(String nodeId) {
		// TODO subscribe to a given nodeid and print the result to the console
		return null;	
	}
	
	private UaNode getUaNodeByString(String nodeId, UaClient client) {
		try {
			return client.getAddressSpace().getNode(NodeId.parseNodeId(nodeId));
		} catch (ServiceException | AddressSpaceException | IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	


}
