package org.htwdd.opcua.client.services;

import org.htwdd.opcua.client.ClientConfigurator;

import com.prosysopc.ua.client.UaClient;

public interface ConnectorService {
	public UaClient connect(ClientConfigurator demo);
}
