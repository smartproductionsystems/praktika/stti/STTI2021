package org.htwdd.opcua.client.services;

import org.htwdd.opcua.client.ClientConfigurator;

public abstract class BaseUaService {

	public ClientConfigurator config;

	public BaseUaService(final ClientConfigurator config) {
		this.config = config;
	}
}
