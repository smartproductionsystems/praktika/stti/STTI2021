package org.htwdd.opcua.client;

import com.prosysopc.ua.client.UaClient;

public interface ClientConfigurator {
	public UaClient configure(UaClient client);
	public UaClient getConfiguredClient();
}
