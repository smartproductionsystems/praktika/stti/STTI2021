package org.htwdd.opcua.client;

import java.net.URISyntaxException;

import org.apache.http.auth.UsernamePasswordCredentials;

import com.prosysopc.ua.ServiceException;
import com.prosysopc.ua.SessionActivationException;
import com.prosysopc.ua.UserIdentity;
import com.prosysopc.ua.client.UaClient;
import com.prosysopc.ua.stack.transport.security.SecurityMode;

public class UserPasswordConfigurer implements ClientConfigurator {

	private final String user;
	private final String password;
	private UaClient client;
	public UserPasswordConfigurer(
			final String user,
			final String password
			){
		this.user = user;
		this.password = password;
	}
	
	@Override
	public UaClient configure(UaClient client) {
		// TODO configure the client with user and password identity
		
		return client;
		
	}

	@Override
	public UaClient getConfiguredClient() {
		return client;
	}

}
