package org.htwdd.opcua.unit.client;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.htwdd.opcua.client.BasicClientConfigurer;
import org.htwdd.opcua.client.ClientConfigurator;
import org.htwdd.opcua.server.BasicServer;
import org.junit.jupiter.api.Test;

import com.prosysopc.ua.ServiceException;
import com.prosysopc.ua.SessionActivationException;
import com.prosysopc.ua.client.ConnectException;
import com.prosysopc.ua.client.InvalidServerEndpointException;
import com.prosysopc.ua.client.UaClient;
import com.prosysopc.ua.server.UaServer;
import com.prosysopc.ua.server.UaServerException;
import com.prosysopc.ua.stack.transport.security.SecurityMode;

public class TestClientConnection {
	
	@Test
	public void ClientShouldConnectToGivenServer() throws UaServerException, InvalidServerEndpointException, ConnectException, SessionActivationException, ServiceException {
				
				// --- GIVEN ---
				// a basic opc ua server
				BasicServer s = new BasicServer(55578,"sttiserver");
				UaServer server = s.configure();
				server.init();
				server.start();
				
				// --- WHEN ---
				// a configured client is returned
				String expected = server.getServerUris()[0];
				ClientConfigurator configuration = new BasicClientConfigurer(server.getServerUris()[0]);				
				configuration.configure(new UaClient());
                UaClient client = configuration.getConfiguredClient();
				client.connect();
				
				
				// --- THEN ---
				// the client should connect to the given server
				assertEquals(true, client.isConnected());
				assertEquals(SecurityMode.NONE, client.getSecurityMode());
				assertEquals(true, client.getAddress().equalsAddress(expected)); 
				client.disconnect();
				server.close();
	}
}
