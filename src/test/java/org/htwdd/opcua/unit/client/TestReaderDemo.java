package org.htwdd.opcua.unit.client;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Matchers.*;

import org.htwdd.opcua.client.ClientConfigurator;
import org.htwdd.opcua.client.services.ReadDataValueService;
import org.junit.jupiter.api.Test;

import com.prosysopc.ua.ServiceException;
import com.prosysopc.ua.StatusException;
import com.prosysopc.ua.client.UaClient;
import com.prosysopc.ua.stack.builtintypes.DataValue;
import com.prosysopc.ua.stack.builtintypes.NodeId;
import com.prosysopc.ua.stack.builtintypes.Variant;

public class TestReaderDemo {
	
	@Test
	public void ReaderShouldReturnValueFromNode() throws ServiceException, StatusException {
		
		// --- GIVEN ---
		// a configured ua client
		ClientConfigurator configuration = mock(ClientConfigurator.class);
		UaClient client = mock(UaClient.class);
		// AND
		// a readable nodeId
		NodeId nodeId = NodeId.parseNodeId("ns=1;s=Test");
		DataValue dv = new DataValue();
		dv.setValue(new Variant(20.3));
		
		// --- WHEN ---
		// the configured client is connected
		when(configuration.getConfiguredClient()).thenReturn(client);
		doNothing().when(client).connect();
		// AND
		// a value was read
		when(client.readValue(any(NodeId.class))).thenReturn(dv);		
		doNothing().when(client).disconnect();
		
		// --- THEN ---
		// the service should return a DataValue containing the expected value from the node
		ReadDataValueService reader = new ReadDataValueService(configuration);		
		DataValue value = reader.readValue("ns=1;s=Test");
		assertEquals(20.3, value.getValue().doubleValue());
		
	}
	
	
}
