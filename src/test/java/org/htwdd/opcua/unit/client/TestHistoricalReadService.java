package org.htwdd.opcua.unit.client;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doNothing;

import org.htwdd.opcua.client.ClientConfigurator;
import org.htwdd.opcua.client.services.HistoricalReadService;
import org.junit.jupiter.api.Test;

import com.prosysopc.ua.ServiceException;
import com.prosysopc.ua.StatusException;
import com.prosysopc.ua.client.ServerConnectionException;
import com.prosysopc.ua.client.UaClient;
import com.prosysopc.ua.stack.builtintypes.DataValue;
import com.prosysopc.ua.stack.encoding.DecodingException;

public class TestHistoricalReadService {

	
	@Test
	public void ReaderShouldReturnValueFromNode() throws ServiceException, StatusException, ServerConnectionException, DecodingException {
		ClientConfigurator client = mock(ClientConfigurator.class);
		UaClient uaClient = mock(UaClient.class);
		
		//GIVEN an historical read endpoint
		DataValue[] expected = new DataValue[1];	
		DataValue v1 = new DataValue();
		expected[0] = v1;
		
		//AND 
		//a configured historyReadRaw call through opc sdk
		when(client.getConfiguredClient()).thenReturn(uaClient);
		/*client.historyReadRaw(NodeId.parseNodeId(nodeId),
					DateTime.MIN_VALUE, 
					DateTime.currentTime(), UnsignedInteger.MAX_VALUE, true, null, TimestampsToReturn.Source);*/
		when(uaClient.historyReadRaw(
					anyObject(),
					anyObject(),
					anyObject(),
					anyObject(),
					anyBoolean(),
					anyObject(),
					anyObject()
				)
		).thenReturn(expected);		
		doNothing().when(uaClient).connect();
		doNothing().when(uaClient).disconnect();
		
		//THEN
		//HistoricalReadService should return an Array of DataValues
		HistoricalReadService history = new HistoricalReadService(client);
		DataValue[] values = history.historicalRead("ns=1;s=Test");	
		assertEquals(1, values.length);
		
	}
}
