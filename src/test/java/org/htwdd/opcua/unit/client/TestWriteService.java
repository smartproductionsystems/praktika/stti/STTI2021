package org.htwdd.opcua.unit.client;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.htwdd.opcua.client.ClientConfigurator;
import org.htwdd.opcua.client.services.WriteService;
import org.junit.jupiter.api.Test;

import com.prosysopc.ua.ServiceException;
import com.prosysopc.ua.StatusException;
import com.prosysopc.ua.client.UaClient;
import com.prosysopc.ua.stack.builtintypes.DataValue;
import com.prosysopc.ua.stack.builtintypes.NodeId;
import com.prosysopc.ua.stack.builtintypes.Variant;

public class TestWriteService {

	@Test
	public void WriterShouldWriteValue() throws ServiceException, StatusException {
		
		// --- GIVEN ---
		// a configured ua client
		ClientConfigurator configuration = mock(ClientConfigurator.class);
		UaClient client = mock(UaClient.class);
		// AND
		// a readable nodeId
		NodeId nodeId = NodeId.parseNodeId("ns=1;s=Test");
		DataValue inputValue = new DataValue();
		inputValue.setValue(new Variant(20.3));
		
		// --- WHEN ---
		// the configured client is connected
		when(configuration.getConfiguredClient()).thenReturn(client);
		doNothing().when(client).connect();
		// AND
		// a value was read
		when(client.writeValue(nodeId, inputValue)).thenReturn(true);		
		doNothing().when(client).disconnect();
		
		// --- THEN ---
		// the service should return a DataValue containing the expected value from the node
		WriteService reader = new WriteService(configuration);
		boolean wasWritten = reader.writeValue("ns=1;s=Test", inputValue);
		assertEquals(true, wasWritten);
		
	}
}
