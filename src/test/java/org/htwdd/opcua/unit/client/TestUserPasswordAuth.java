package org.htwdd.opcua.unit.client;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.htwdd.opcua.client.BasicClientConfigurer;
import org.htwdd.opcua.client.UserPasswordConfigurer;
import org.htwdd.opcua.client.ClientConfigurator;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.*;

import com.prosysopc.ua.SessionActivationException;
import com.prosysopc.ua.client.UaClient;


public class TestUserPasswordAuth {
	
	@Test
	public void AccessShouldBeGrantedOnlyWithPassword() throws SessionActivationException {
		// ---- GIVEN ----
		//a registered user
		final String USER = "alf";
		final String PASSWORD = "melmac";
		
		// AND 
		// a given UAClient
		UaClient client = new UaClient();
		
		// ---- WHEN ----
		// a UserPasswordConfigurer returns a configured UaClient
		ClientConfigurator con = new UserPasswordConfigurer(USER, PASSWORD);		
		client = con.configure(client);
		
		// ---- THEN ----
		// the client should contain a correct UserIdentity
		assertEquals(USER, client.getUserIdentity().getName());
		assertEquals(PASSWORD, client.getUserIdentity().getPassword());
	}
}
