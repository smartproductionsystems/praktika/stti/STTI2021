package org.htwdd.opcua.unit.client;

import org.htwdd.opcua.client.BasicClientConfigurer;
import org.htwdd.opcua.client.ClientConfigurator;
import org.htwdd.opcua.client.services.SubscriptionService;
import org.htwdd.opcua.server.BasicServer;
import org.htwdd.opcua.server.util.AddVariable;
import org.junit.jupiter.api.Test;

import com.prosysopc.ua.ServiceException;
import com.prosysopc.ua.SessionActivationException;
import com.prosysopc.ua.client.ConnectException;
import com.prosysopc.ua.client.InvalidServerEndpointException;
import com.prosysopc.ua.client.UaClient;
import com.prosysopc.ua.server.UaServer;
import com.prosysopc.ua.server.UaServerException;
import com.prosysopc.ua.stack.core.Identifiers;

public class TestSubscriptionValue {

	@Test
	public void ClientShouldReturnReadableValues() throws UaServerException, InvalidServerEndpointException, ConnectException, SessionActivationException, ServiceException {
		
		// ---- GIVEN ----
		//a basic server
		BasicServer s = new BasicServer(55579,"test");
		UaServer server = s.configure();
		server.init();
		server.start();
		//AND
		//a subscribable variable on the server
		AddVariable addVariable = new AddVariable(server.getNodeManagerRoot(),server.getNodeManagerRoot().getObjectsFolder().getNodeId());
		addVariable.addVariable("Test", 1.0, Identifiers.Double);
		
		// ---- WHEN----
		//a configured client connects to the test server
		ClientConfigurator configuration = new BasicClientConfigurer(server.getServerUris()[0]);	
		configuration.configure(new UaClient());		
		//AND a subscription has been added		
		SubscriptionService reader = new SubscriptionService(configuration);		
		reader.subscribe("s=Test");	
		
		// ---- THEN ----
		//the client subscription count should reflect the amount of subscriptions made
		assert configuration.getConfiguredClient().getSubscriptionCount() == 1;		
		configuration.getConfiguredClient().disconnect();
	}
	
	
	
	
	
}
