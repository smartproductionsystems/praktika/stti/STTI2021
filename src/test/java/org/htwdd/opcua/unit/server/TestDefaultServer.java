package org.htwdd.opcua.unit.server;




import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.net.UnknownHostException;

import org.htwdd.opcua.server.DefaultServer;
import org.htwdd.opcua.server.NoSecurityConfig;
import org.junit.jupiter.api.Test;

import com.prosysopc.ua.SecureIdentityException;
import com.prosysopc.ua.server.UaServer;
import com.prosysopc.ua.server.UaServerException;
import com.prosysopc.ua.stack.transport.security.SecurityMode;
import com.prosysopc.ua.stack.utils.EndpointUtil;

public class TestDefaultServer {
	
	@Test
	public void testServerConfiguration() throws UaServerException, UnknownHostException, SecureIdentityException, IOException {
		
		//Given
		int port = 50000;
		String serverName = "stti";
		DefaultServer b = new DefaultServer(port,serverName);
		NoSecurityConfig config = new NoSecurityConfig(
				port,
				serverName,
				EndpointUtil.getInetAddresses(true)
				);
		
		
		//When
		UaServer server = b.configure();		
		
	
		//Then
		assertEquals(serverName,server.getServerName());
		assertEquals(port, server.getPort());
		assertEquals(server.getBindAddresses(), config.inetAddress);
		assertTrue(server.getSecurityModes().contains(SecurityMode.NONE));
		assertEquals(server.getUserTokenPolicies()[0], config.userTokenPolicy);
		assertNotNull(server.getApplicationIdentity());
		
		
	}	
	
	
}
