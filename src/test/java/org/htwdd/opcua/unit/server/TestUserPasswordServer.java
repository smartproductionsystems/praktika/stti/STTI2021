package org.htwdd.opcua.unit.server;

import java.util.Arrays;

import org.htwdd.opcua.server.BasicServer;
import org.htwdd.opcua.server.UserPasswordServerWrapper;
import org.junit.jupiter.api.Test;

import com.prosysopc.ua.ServiceException;
import com.prosysopc.ua.SessionActivationException;
import com.prosysopc.ua.UserIdentity;
import com.prosysopc.ua.UserTokenPolicies;
import com.prosysopc.ua.client.ConnectException;
import com.prosysopc.ua.client.InvalidServerEndpointException;
import com.prosysopc.ua.client.UaClient;
import com.prosysopc.ua.server.UaServer;
import com.prosysopc.ua.server.UaServerException;
import com.prosysopc.ua.stack.transport.security.SecurityMode;


public class TestUserPasswordServer {

	
		
	@Test
	public void testUserPasswordPolicy() throws UaServerException, InvalidServerEndpointException, ConnectException, SessionActivationException, ServiceException {
		
		//given
		int port = 50000;
		String serverName = "stti";		
		String allowedUser = "john";
		String allowedPasword = "doe";
		UserPasswordServerWrapper userPassword = new UserPasswordServerWrapper(
					new BasicServer(
							port,
							serverName
						)
				);
		
		//when
		UaServer server = userPassword.configure();
		server.start();
		
		//then
		Arrays.asList(server.getUserTokenPolicies()).contains(UserTokenPolicies.SECURE_USERNAME_PASSWORD);
		Arrays.asList(server.getUserTokenPolicies()).contains(UserTokenPolicies.ANONYMOUS);		
		//AND
		UaClient client = new UaClient(server.getServerUris()[0]);
		client.setSecurityMode(SecurityMode.NONE);
		client.setUserIdentity(new UserIdentity(allowedUser, allowedPasword));
		client.connect();
		
	}
}
