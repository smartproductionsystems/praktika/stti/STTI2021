package org.htwdd.opcua.unit.server;

import static org.junit.Assert.assertTrue;

import org.htwdd.opcua.lighting.ObjectTypeIds;
import org.htwdd.opcua.server.BasicServer;
import org.htwdd.opcua.server.DefaultServer;
import org.htwdd.opcua.server.XMLLoader;
import org.junit.jupiter.api.Test;

import com.prosysopc.ua.server.UaServer;

public class TestXMLLoader {

	
	@Test
	public void testXMLLoader() {
		
		//given
		int port = 50000;
		String serverName = "stti";
		String xmlFile = "lighting.xml";
		
		//when
		XMLLoader loader = new XMLLoader(new BasicServer(port,serverName));
		UaServer server = loader.ImportXMLModel(xmlFile);
		
		//then
		assertTrue(server.getRegisteredClasses().containsClass(ObjectTypeIds.Lighter));
		
		
	}
}
