package org.htwdd.opcua.unit.server;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.htwdd.opcua.server.BasicServer;
import org.htwdd.opcua.server.GenClassInstanceService;
import org.junit.jupiter.api.Test;

import com.prosysopc.ua.server.UaServer;
import com.prosysopc.ua.stack.builtintypes.QualifiedName;


public class TestCreateInstance {

	
	@Test
	public void creatEinstance() {
		//given
		BasicServer basicServer = new BasicServer(50000, "stti");
		String instanceName = "MyLighter";
		
		//when
		GenClassInstanceService service = new GenClassInstanceService(basicServer, instanceName);
		UaServer server = service.configure();
		
		//then
		QualifiedName expectedName = new QualifiedName(instanceName);
		assertEquals(
				expectedName,
				server
					.getAddressSpace()
					.getNodeManagerRoot()
					.getObjectsFolder()
					.getComponents()[0]
						.getBrowseName()
		);
		
		
	}
}
